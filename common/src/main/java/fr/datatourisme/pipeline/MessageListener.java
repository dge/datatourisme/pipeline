/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline;

import org.apache.jena.ext.com.google.common.collect.ImmutableList;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.util.MonitorModel;
import org.seaborne.patch.RDFChanges;
import org.seaborne.patch.RDFPatchOps;
import org.seaborne.patch.changes.PatchSummary;
import org.seaborne.patch.changes.RDFChangesWriter;
import org.seaborne.patch.text.TokenWriterText;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.BatchMessageListener;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * This message listener build a monitor model with the messages content, pass it to a model processor
 * then compute additions & deletions to build a RDF patch witch will be send into a AMQB queue.
 */
public class MessageListener implements BatchMessageListener {
    protected static final Logger LOG = LoggerFactory.getLogger(MessageListener.class);
    private static final String SINK_QUEUE_NAME = "datatourisme.pipeline.sink";

    private final RabbitTemplate rabbitTemplate;
    private final ModelProcessor modelProcessor;

    public MessageListener(RabbitTemplate rabbitTemplate, ModelProcessor modelProcessor) {
        this.rabbitTemplate = rabbitTemplate;
        this.modelProcessor = modelProcessor;
    }

    @Override
    public void onMessage(Message message) {
        onMessageBatch(ImmutableList.of(message));
    }

    @Override
    public void onMessageBatch(List<Message> messages) {
        Model model = ModelFactory.createDefaultModel();
        for (Message message : messages) {
            if (message.getBody().length > 0) {
                // load into model
                try {
                    ByteArrayInputStream bis = new ByteArrayInputStream(message.getBody());
                    RDFDataMgr.read(model, bis, Lang.NTRIPLES);
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }
        }

        if (model.size() > 0) {
            // info
            LOG.info("Received model from pipeline source : {} statements", model.size());

            // create a monitor model, snapshot then process
            MonitorModel monitorModel = new MonitorModel(model);
            monitorModel.snapshot();
            modelProcessor.process(monitorModel);

            // commit the model
            commit(monitorModel);
        }
    }

    @Override
    public void containerAckMode(AcknowledgeMode mode) {
        // @todo ?
    }

    /**
     * Compute additions & deletions to build a RDF patch and send it
     *
     * @param model
     */
    protected void commit(MonitorModel model) {
        // extract model snapshot
        List<Statement> additions = new ArrayList<>();
        List<Statement> deletions = new ArrayList<>();
        model.snapshot(additions, deletions);

        // prepare patch
        StringWriter writer = new StringWriter();
        RDFChanges patch = new RDFChangesWriter(new TokenWriterText(writer));
        Node graphNode = NodeFactory.createURI(modelProcessor.getGraphUri());

        if (additions.size() == 0 && deletions.size() == 0) {
            return;
        }

        // add begin
        patch.txnBegin();

        // add additions to patch
        additions.forEach(stmt -> {
            patch.add(graphNode, stmt.getSubject().asNode(), stmt.getPredicate().asNode(), stmt.getObject().asNode());
        });

        // add deletions to patch
        deletions.forEach(stmt -> {
            patch.delete(graphNode, stmt.getSubject().asNode(), stmt.getPredicate().asNode(), stmt.getObject().asNode());
        });

        // add commit
        patch.txnCommit();

        // log
        PatchSummary summary = RDFPatchOps.summary(RDFPatchOps.read(new ByteArrayInputStream(writer.toString().getBytes())));
        LOG.info("Send patch in pipeline sink : {} ADD, {} DELETE", summary.countAddData, summary.countDeleteData);

        // send
        MessageProperties properties = new MessageProperties();
        Message message = new Message(writer.toString().getBytes(), properties);
        rabbitTemplate.send(SINK_QUEUE_NAME, message);
        LOG.debug(writer.toString());
    }
}