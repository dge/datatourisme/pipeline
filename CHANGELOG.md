CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## [1.0.0] - 2020-11-20

### Ajout
- Finalisation et déploiement du projet du projet
- Ajout du CHANGELOG
