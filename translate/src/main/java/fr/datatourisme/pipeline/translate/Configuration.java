/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.translate;

import fr.datatourisme.pipeline.ModelProcessor;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;


@org.springframework.context.annotation.Configuration
@Import({ fr.datatourisme.pipeline.Configuration.class })
@EnableConfigurationProperties(ApplicationProperties.class)
public class Configuration {

    @Bean
    ModelProcessor messageProcessor(DeeplTranslateService translateService, ApplicationProperties applicationProperties) {
        return new TranslateModelProcessor(translateService, applicationProperties);
    }

    @Bean
    public DeeplTranslateService translateService(@Value( "${cache.db.path}" ) String cacheDbPath, @Value( "${deepl.authkey}" ) String authKey) {
        DB db = DBMaker.fileDB(cacheDbPath).fileMmapEnable().transactionEnable().make();
        return new DeeplTranslateService(authKey, db);
    }
}
