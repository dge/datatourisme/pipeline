/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.translate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.mapdb.DB;
import org.mapdb.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DeeplTranslateService {
    private static final Logger LOG = LoggerFactory.getLogger(DeeplTranslateService.class);
    public static final String URI = "https://api.deepl.com/v2/translate";

    private final String authKey;
    private final DB db;
    private final Map<String, String> cache;
    private final CloseableHttpClient httpClient;

    public DeeplTranslateService(String authKey, DB db) {
        this.authKey = authKey;
        this.db = db;
        this.cache = db.hashMap("map", Serializer.STRING, Serializer.STRING).createOrOpen();

        RequestConfig defaultRequestConfig = RequestConfig.custom()
            .setConnectTimeout(30 * 1000)
            .setSocketTimeout(60 * 1000)
            .build();

        this.httpClient = HttpClientBuilder.create()
            .setDefaultRequestConfig(defaultRequestConfig)
            .build();
    }

    /**
     * @param texts
     * @param sourceLanguage
     * @param targetLanguage
     * @return
     */
    public Map<String, String> translate(List<String> texts, Language sourceLanguage, Language targetLanguage) {
        // fill the translations from cache
        Map<String, String> translations = texts.stream()
            .collect(HashMap::new, (m,text)->m.put(text, cache.getOrDefault(getCacheKey(text, sourceLanguage, targetLanguage), null)), HashMap::putAll);

        // find the missing values to build the list to translate
        List<String> sources =
            translations.entrySet().stream()
            .filter(e -> Objects.isNull(e.getValue()))
            .map(Map.Entry::getKey)
            .distinct()
            .collect(Collectors.toList());

        if (sources.size() > 0) {
            // translate
            Map<String, String> results = doTranslate(sources, sourceLanguage, targetLanguage);

            // foreach result, add to response and save to cache
            results.forEach((source, translation) -> {
                translations.put(source, translation);
                if (translation != null) {
                    String key = getCacheKey(source, sourceLanguage, targetLanguage);
                    cache.put(key, translation);
                }
            });

            // commit cache update
            db.commit();
        }

        return translations;
    }

    /**
     * Get the cache key for the given text and languages
     */
    private String getCacheKey(String text, Language sourceLanguage, Language targetLanguage) {
        return String.format("%s_%s_%s", sourceLanguage.toString().toLowerCase(), DigestUtils.md5Hex(text), targetLanguage.toString().toLowerCase());
    }

    /**
     * Perform an API call to get the translation
     *
     * @param sourceLanguage
     * @param targetLanguage
     * @return
     */
    private Map<String, String> doTranslate(List<String> texts, Language sourceLanguage, Language targetLanguage) {
        // partition the texts list to meet the deepl requirements (max 50 parameters)
        if (texts.size() > 50) {
            return Lists.partition(texts, 50).stream()
                .flatMap(list -> doTranslate(list, sourceLanguage, targetLanguage).entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        }

        // prepare translations map
        Map<String, String> translations = new HashMap<>(texts.size());

        // query deepl
        try {
            URIBuilder builder = new URIBuilder(URI)
                .setParameter("auth_key", this.authKey)
                .setParameter("source_lang", sourceLanguage.toString())
                .setParameter("target_lang", targetLanguage.toString())
                .setParameter("preserve_formatting", "1");

            texts.forEach(text -> builder.addParameter("text", text));

            LOG.info("Request to the DeepLAPI for {} to {} : {} texts", sourceLanguage, targetLanguage, texts.size());
            HttpGet request = new HttpGet(builder.build());
            CloseableHttpResponse response = httpClient.execute(request);
            try {
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() != 200) {
                    throw new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
                }

                HttpEntity entity = response.getEntity();
                String result = EntityUtils.toString(entity);
                ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                APIResponse apiResponse = mapper.readValue(result, APIResponse.class);

                // populate map
                for (int i = 0; i < texts.size(); i++) {
                    translations.put(texts.get(i), apiResponse.translations.get(i).text);
                }

            } finally {
                response.close();
            }
        } catch (URISyntaxException | IOException e) {
            LOG.error(e.getMessage(), e);
        }

        // return map
        return translations;
    }

    /**
     * API Response mapping class
     */
    public static class APIResponse {
        @JsonProperty("translations")
        public List<Translation> translations;

        public static class Translation {
            @JsonProperty("text")
            public String text;
        }
    }
}