/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata.reconciliation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

public class ReconciliationQuery {

    @JsonProperty("query")
    protected String query;

    @JsonProperty("type")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected List<String> types;

    @JsonProperty("type_strict")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String isTypeStrict() {
        if(types != null) {
            return "should";
        }
        return null;
    }

    @JsonProperty("properties")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected List<QueryProperty> properties;

    // Only send limit if it's non-default (default = 0) to preserve backward
    // compatibility with services which might choke on this (pre-2013)
    @JsonProperty("limit")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected int limit;
//
//    public ReconciliationQuery() {
//        super();
//        this.query = "";
//        this.typeID = null;
//        this.properties = null;
//        this.limit = 0;
//    }

    @JsonCreator
    public ReconciliationQuery(String query, List<String> types, List<QueryProperty> properties, int limit) {
        this.query = query;
        this.types = types;
        this.properties = properties;
        this.limit = limit;
    }

    @Override
    public String toString() {
        try {
            return ParsingUtilities.mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return super.toString();
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    protected static class QueryProperty {
        @JsonProperty("pid")
        String pid;
        @JsonProperty("v")
        Object v;

        protected QueryProperty(
                String pid,
                Object v) {
            this.pid = pid;
            this.v = v;
        }

        @Override
        public String toString() {
            try {
                return ParsingUtilities.mapper.writeValueAsString(this);
            } catch (JsonProcessingException e) {
                return super.toString();
            }
        }
    }

    public static class Builder {
        private String query = "";
        private final List<String> types = new ArrayList<>();
        private final List<QueryProperty> properties = new ArrayList<>();
        private int limit = 5;

        public Builder() {
        }

        public Builder setQuery(String query) {
            this.query = query;
            return this;
        }

        public Builder addType(String type) {
            this.types.add(type);
            return this;
        }

        public Builder setLimit(int limit) {
            this.limit = limit;
            return this;
        }

        public Builder addProperty(String pid, Object v) {
            this.properties.add(new QueryProperty(pid, v));
            return this;
        }

        public ReconciliationQuery build() {
            return new ReconciliationQuery(query, types, properties, limit);
        }
    }
}
