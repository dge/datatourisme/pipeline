/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata;

import fr.datatourisme.pipeline.ModelProcessor;
import fr.datatourisme.pipeline.wikidata.reconciliation.ReconciliationServiceApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@org.springframework.context.annotation.Configuration
@Import({ fr.datatourisme.pipeline.Configuration.class })
public class Configuration {
    @Bean
    public ReconciliationServiceApi reconciliationServiceApi(@Value( "${reconciliation.service.endpoint}" ) String serviceEndpointUrl) {
        return new ReconciliationServiceApi(serviceEndpointUrl);
    }

    @Bean
    public ReconciliationHelper reconciliationHelper(ReconciliationServiceApi reconciliationServiceApi) {
        return new ReconciliationHelper(reconciliationServiceApi);
    }

    @Bean
    ModelProcessor modelProcessor(ReconciliationHelper reconciliationHelper) {
        return new WikidataModelProcessor(reconciliationHelper);
    }
}
