/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata;

import fr.datatourisme.pipeline.utils.JenaUtils;
import fr.datatourisme.pipeline.vocabulary.Datatourisme;
import fr.datatourisme.pipeline.vocabulary.Schema;
import fr.datatourisme.pipeline.vocabulary.WikidataEntity;
import fr.datatourisme.pipeline.vocabulary.WikidataPropDirect;
import fr.datatourisme.pipeline.wikidata.reconciliation.Reconciliation;
import fr.datatourisme.pipeline.wikidata.reconciliation.ReconciliationCandidate;
import fr.datatourisme.pipeline.wikidata.reconciliation.ReconciliationQuery;
import fr.datatourisme.pipeline.wikidata.reconciliation.ReconciliationServiceApi;
import fr.datatourisme.pipeline.wikidata.utils.GeoUtils;
import org.apache.jena.arq.querybuilder.ConstructBuilder;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.util.SplitIRI;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ReconciliationHelper {
    private static final Logger LOG = LoggerFactory.getLogger(ReconciliationHelper.class);

    private final ReconciliationServiceApi serviceApi;

    public ReconciliationHelper(ReconciliationServiceApi serviceApi) {
        this.serviceApi = serviceApi;
    }

    /**
     * Reconcile a Point of Interest
     *
     * @param resource
     * @return
     */
    public Resource reconcile(Resource resource) {
        // search existing owl:sameAs triple
        Optional<Resource> optionalResource = resource.listProperties(OWL.sameAs)
                .filterKeep(stmt -> stmt.getObject().asResource().getURI().startsWith(WikidataEntity.getURI()))
                .mapWith(stmt -> stmt.getObject().asResource())
                .nextOptional();

        if (optionalResource.isPresent()) {
            // found, return it
            String id = SplitIRI.localname(optionalResource.get().getURI());
            return fetchWikidataResource(id);
        }

        // make a query to the reconciliation API
        return reconcileQuery(resource);
    }

    /**
     *
     * @return
     */
    private Resource reconcileQuery(Resource resource) {
        // Prepare the reconciliation api
        ReconciliationQuery.Builder builder = ReconciliationQuery.builder()
            .addType("Q2221906") // geographic location
            .setLimit(5);

        // Label
        String label = resource.getProperty(RDFS.label, "fr").getString();
        builder.setQuery(normalizeLabel(label));

        // Country : France
        builder.addProperty("P17", "Q142");

        // Insee code : dt:isLocatedAt/schema:address/dt:hasAddressCity
        RDFNode node = JenaUtils.getPropertySequenceValue(resource, Datatourisme.isLocatedAt, Schema.address, Datatourisme.hasAddressCity);
        if (node != null && node.isResource()) {
            String inseeCode = SplitIRI.localname(node.asResource().getURI());
            builder.addProperty("P131/P374", inseeCode);
        }

        // execute reconciliation query
        ReconciliationQuery query = builder.build();
        Reconciliation reconciliation = serviceApi.execute(query);
        if (reconciliation == null || reconciliation.getCandidates().size() == 0) {
            // no candidate, return
            return null;
        }

        // if best candidate has a perfect match on location, return it
        ReconciliationCandidate.Feature feature = reconciliation.getBestCandidate().getFeature("P131/P374");
        if(feature != null && Integer.parseInt(feature.value.toString()) == 100) {
            Resource wikidataResource = fetchWikidataResource(reconciliation.getBestCandidate().id);
            if (wikidataResource != null) {
                LOG.info("Reconciliation by locality : {} -> {}", label, wikidataResource.getURI());
            }
            return wikidataResource;
        }

        // limit to the closest ones (dt:isLocatedAt/schema:geo)
        node = JenaUtils.getPropertySequenceValue(resource, Datatourisme.isLocatedAt, Schema.geo);
        if (node != null && node.isResource()) {
            Statement latStmt = node.asResource().getProperty(Schema.latitude);
            Statement lonStmt = node.asResource().getProperty(Schema.longitude);
            if (latStmt != null && lonStmt != null) {
                // prepare latitude/longitude
                double latitude = latStmt.getDouble();
                double longitude = lonStmt.getDouble();
                GeoUtils.LatLng latLng = GeoUtils.latLng(latitude, longitude);

                // get all candidates
                String[] ids = reconciliation.getCandidates().stream().map(candidate -> candidate.id).toArray(String[]::new);
                List<Resource> candidates = fetchWikidataResources(ids);

                // keep only ones that distance < 10km
                Optional<Resource> optional = candidates.stream()
                    .filter(res -> res.hasProperty(WikidataPropDirect.coordinateLocation))
//                    .sorted(Comparator.comparingDouble(res -> {
//                        String point = res.getProperty(WikidataPropDirect.coordinateLocation).getObject().asLiteral().getString();
//                        return GeoUtils.distance(latLng, GeoUtils.parseWKTPoint(point));
//                    }))
                    .filter(res -> {
                        String point = res.getProperty(WikidataPropDirect.coordinateLocation).getObject().asLiteral().getString();
                        double distance = GeoUtils.distance(latLng, GeoUtils.parseWKTPoint(point));
                        return distance < 10000;
                    })
                    .findFirst();

                if (optional.isPresent()) {
                    // found, return it
                    Resource wikidataResource = optional.get();
                    LOG.info("Reconciliation by distance : {} -> {}", label, wikidataResource.getURI());
                    return wikidataResource;
                }
            }
        }

        // if best candidate is a match, return it
        if (reconciliation.getBestCandidate().match) {
            Resource wikidataResource = fetchWikidataResource(reconciliation.getBestCandidate().id);
            LOG.info("Reconciliation by match : {} -> {}", label, wikidataResource.getURI());
            return wikidataResource;
        }

        return null;
    }

    /**
     * Normalize label to help reconciliation process
     * Ex: Le Bassin d'Arcachon => Bassin d'Arcachon
     *
     * @param label
     * @return
     */
    private String normalizeLabel(String label) {
        label = label.toLowerCase().startsWith("le ") ? label.substring("le ".length()) : label;
        label = label.toLowerCase().startsWith("la ") ? label.substring("la ".length()) : label;
        label = label.toLowerCase().startsWith("les ") ? label.substring("les ".length()) : label;
        return label;
    }

    /**
     * Get the wikidata resource for the given wikidata uri
     *
     * @return
     */
    private Resource fetchWikidataResource(String id) {
        List<Resource> list = fetchWikidataResources(id);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    /**
     * Get the wikidata ressources for the given uris
     *
     * @param ids
     * @return
     */
    private List<Resource> fetchWikidataResources(String... ids) {
        /**
         * CONSTRUCT { ?s ?p ?o } WHERE {?s ?p ?o. VALUES ?s { <...> } }
         */
        Resource[] values = Arrays.stream(ids)
            .map(WikidataEntity::resource)
            .toArray(Resource[]::new);

        ConstructBuilder construct = new ConstructBuilder();
        Triple triple = new Triple(Var.alloc("s"), Var.alloc("p"), Var.alloc("o"));
        construct.addConstruct(triple);
        construct.addWhere(triple);
        construct.addValueVar("s", values);

        try (RDFConnection conn = RDFConnectionWikidata.create().build()) {
            Model model = conn.queryConstruct(construct.build());
            return Arrays.stream(ids)
                .map(id -> model.getResource(WikidataEntity.resource(id).getURI()))
                .collect(Collectors.toCollection(ArrayList::new));
        }
    }
}
