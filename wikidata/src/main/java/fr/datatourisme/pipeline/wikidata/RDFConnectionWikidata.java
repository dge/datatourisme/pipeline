/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata;

import org.apache.http.client.HttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionRemote;
import org.apache.jena.rdfconnection.RDFConnectionRemoteBuilder;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.resultset.ResultSetLang;
import org.apache.jena.sparql.core.Transactional;

import java.util.function.Function;

/**
 * RDFConnectionRemote for Wikidata
 */
public class RDFConnectionWikidata extends RDFConnectionRemote {
    public RDFConnectionWikidata(Transactional txnLifecycle, HttpClient httpClient, HttpContext httpContext, String destination, String queryURL, String updateURL, String gspURL, RDFFormat outputQuads, RDFFormat outputTriples, String acceptDataset, String acceptGraph, String acceptSparqlResults, String acceptSelectResult, String acceptAskResult, boolean parseCheckQueries, boolean parseCheckUpdates) {
        super(txnLifecycle, httpClient, httpContext, destination, queryURL, updateURL, gspURL, outputQuads, outputTriples, acceptDataset, acceptGraph, acceptSparqlResults, acceptSelectResult, acceptAskResult, parseCheckQueries, parseCheckUpdates);
    }

    public static RDFConnectionRemoteBuilder create() {
        return setupForWikidata(RDFConnectionRemote.create());
    }

    private static RDFConnectionRemoteBuilder setupForWikidata(RDFConnectionRemoteBuilder builder) {
        return builder.destination("https://query.wikidata.org/sparql");
    }
}
