/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata.reconciliation;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.jena.ext.com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Simple implementation of https://reconciliation-api.github.io/specs/latest/
 */
public class ReconciliationServiceApi {
    private static final Logger LOG = LoggerFactory.getLogger(ReconciliationServiceApi.class);

    private final String endpointUrl;
    private final CloseableHttpClient httpClient;

    public ReconciliationServiceApi(String endpointUrl) {
        this.endpointUrl = endpointUrl;

        RequestConfig defaultRequestConfig = RequestConfig.custom()
            .setConnectTimeout(5 * 1000)
            .setSocketTimeout(5 * 1000)
            .build();

        this.httpClient = HttpClientBuilder.create()
            .setDefaultRequestConfig(defaultRequestConfig)
            .build();
    }

    /**
     * @param query
     * @return
     */
    public Reconciliation execute(ReconciliationQuery query) {
        List<ReconciliationQuery> queries = ImmutableList.of(query);
        List<Reconciliation> results = execute(queries);
        if (results.size() > 0) {
            return results.get(0);
        }
        return null;
    }

    /**
     * @param queries
     * @return
     */
    public List<Reconciliation> execute(List<ReconciliationQuery> queries) {
        StringWriter stringWriter = new StringWriter();
        List<Reconciliation> results = new ArrayList<>(queries.size());

        stringWriter.write("{");
        for (int i = 0; i < queries.size(); i++) {
            ReconciliationQuery query = queries.get(i);
            if (i > 0) {
                stringWriter.write(",");
            }
            stringWriter.write("\"q" + i + "\":");
            stringWriter.write(query.toString());
        }
        stringWriter.write("}");
        String queriesString = stringWriter.toString();

        HttpPost request = new HttpPost(endpointUrl);
        List<NameValuePair> body = Collections.singletonList(new BasicNameValuePair("queries", queriesString));
        request.setEntity(new UrlEncodedFormEntity(body, Consts.UTF_8));

        try (CloseableHttpResponse response = httpClient.execute(request)) {
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() >= 400) {
                throw new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
            }

            HttpEntity entity = response.getEntity();
            String s = EntityUtils.toString(entity);
            ObjectNode o = (ObjectNode) ParsingUtilities.mapper.readTree(s);

            for (int i = 0; i < queries.size(); i++) {
                String key = "q" + i;
                Reconciliation recon = new Reconciliation();
                if (o.has(key) && o.get(key) instanceof ObjectNode) {
                    ObjectNode o2 = (ObjectNode) o.get(key);
                    if (o2.has("result") && o2.get("result") instanceof ArrayNode) {
                        ArrayNode nodes = (ArrayNode) o2.get("result");
                        // Sort results by decreasing score
                        ParsingUtilities.mapper.convertValue(nodes, new TypeReference<List<ReconciliationCandidate>>() {}).stream()
                            .sorted((a, b) -> Double.compare(b.score, a.score))
                            .forEach(recon::addCandidate);
                    }
                }
                results.add(recon);
            }
        } catch (Exception e) {
            LOG.error("Failed to execute reconciliation :\n" + queriesString, e);
        }

        return results;
    }

}
