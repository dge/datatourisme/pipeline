/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata;

import com.fasterxml.jackson.databind.JsonNode;
import fr.datatourisme.pipeline.utils.JenaUtils;
import fr.datatourisme.pipeline.ModelProcessor;
import fr.datatourisme.pipeline.vocabulary.*;
import fr.datatourisme.pipeline.wikidata.mediawiki.MediaWikiActionApi;
import fr.datatourisme.pipeline.wikidata.reconciliation.Reconciliation;
import fr.datatourisme.pipeline.wikidata.reconciliation.ReconciliationCandidate;
import fr.datatourisme.pipeline.wikidata.reconciliation.ReconciliationQuery;
import fr.datatourisme.pipeline.wikidata.reconciliation.ReconciliationServiceApi;
import org.apache.jena.arq.querybuilder.ConstructBuilder;
import org.apache.jena.ext.com.google.common.collect.ImmutableList;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.util.MonitorModel;
import org.apache.jena.util.ResourceUtils;
import org.apache.jena.util.SplitIRI;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.XSD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WikidataModelProcessor implements ModelProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(WikidataModelProcessor.class);
    private final List<Resource> eligibleClasses = ImmutableList.of(Datatourisme.CulturalSite, Datatourisme.NaturalHeritage);
    private final ReconciliationHelper reconciliationHelper;

    public WikidataModelProcessor(ReconciliationHelper reconciliationHelper) {
        this.reconciliationHelper = reconciliationHelper;
    }

    @Override
    public String getGraphUri() {
        return "https://www.wikidata.org";
    }

    /**
     * Process a message
     */
    public void process(MonitorModel model) {
        // clean model for existing wikidata related resources
        cleanModel(model);

        // process each eligible resource
        model.listSubjectsWithProperty(RDF.type, Datatourisme.PointOfInterest)
            .filterKeep(res -> eligibleClasses.stream().anyMatch(c -> res.hasProperty(RDF.type, c)))
            .forEachRemaining(this::processResource);
    }

    /**
     * Clean model for existing wikidata related resources
     *
     * @param model
     */
    private void cleanModel(Model model) {
        // get all ebucore:EditorialObject linked to wikimedia file
        List<Resource> resources = model.listStatements(null, Ebucore.locator, (RDFNode) null).toList().stream()
            .filter(statement -> statement.getObject().asLiteral().getString().startsWith("https://upload.wikimedia.org/"))
            .flatMap(statement -> model.listStatements(null, null, statement.getSubject()).toList().stream())
            .map(Statement::getSubject)
            .collect(Collectors.toList());

        // foreach, remove all related statements
        resources.forEach(resource -> {
            model.remove(JenaUtils.getRootedSubModel(resource));
            model.remove(model.listStatements(null, null, resource));
        });
    }

    /**
     * Process a resource
     *
     * @return
     */
    private void processResource(Resource resource) {
        // reconcile with wikidata
        Resource wikidataResource = getWikidataResource(resource);
        if (wikidataResource != null) {
            Model wikidataModel = fetchWikidataModel(wikidataResource);
            // for each image
            wikidataModel.listObjectsOfProperty(WikidataPropDirect.image).forEachRemaining(node -> {
                addEditorialObject(resource, node.asResource().getURI());
            });
        }
    }

    /**
     * Add a media from Wikimedia Commons
     *
     * @return
     */
    private void addEditorialObject(Resource resource, String fileUri) {
        try {
            // prepare filename
            String filename = fileUri.replace("Special:FilePath/", "File:");
            filename = filename.substring(filename.indexOf("File:"));
            filename = URLDecoder.decode(filename, StandardCharsets.UTF_8.toString());

            // ask for metadata
            JsonNode metadata = MediaWikiActionApi.WIKIPEDIA.imageinfo(filename, "extmetadata", "url", "dimensions", "mime", "size");
            if (metadata != null) {
                Resource editorialObject = createEditorialObjectResource(metadata, resource.getModel());
                if (editorialObject != null) {
                    resource.addProperty(Datatourisme.hasRepresentation, editorialObject);
                }
            }
        } catch (UnsupportedEncodingException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * Get the sameAs wikidata resource for the given resource. Use the reconcile
     * API if the triple not exists
     *
     * @param resource
     * @return
     */
    private Resource getWikidataResource(Resource resource) {
        // search existing triple
        Optional<Resource> optionalResource = resource.listProperties(OWL.sameAs)
            .filterKeep(stmt -> stmt.getObject().asResource().getURI().startsWith(WikidataEntity.getURI()))
            .mapWith(stmt -> stmt.getObject().asResource())
            .nextOptional();

        if (optionalResource.isPresent()) {
            // found, return it
            return optionalResource.get();
        }

        // try to reconcile
        Resource wikidataResource = reconciliationHelper.reconcile(resource);
        if (wikidataResource != null) {
            resource.addProperty(OWL.sameAs, wikidataResource);
            // @todo : send link to wikidata
            return wikidataResource;
        }

        return null;
    }

    /**
     * Get the wikidata model for the given wikidata resource
     *
     * @param resource
     * @return
     */
    private Model fetchWikidataModel(Resource resource) {
        /**
         * CONSTRUCT { ?s ?p ?o } WHERE {?s ?p ?o. VALUES ?s { <...> } }
         */
        ConstructBuilder construct = new ConstructBuilder();
        Triple triple = new Triple(Var.alloc("s"), Var.alloc("p"), Var.alloc("o"));
        construct.addConstruct(triple);
        construct.addWhere(triple);
        construct.addValueVar("s", resource);

        try (RDFConnection conn = RDFConnectionWikidata.create().build()) {
            return conn.queryConstruct(construct.build());
        }
    }

    /**
     * Create a new EditorialObject resource into the given model
     *
     * @param metadata
     * @param model
     * @return
     */
    private Resource createEditorialObjectResource(JsonNode metadata, Model model) {
        JsonNode extmetadata = metadata.get("extmetadata");
        if (extmetadata != null) {
            // ---
            // ebucore:Resource
            // ---
            Resource ebucoreResource = model.createResource();
            ebucoreResource.addProperty(RDF.type, Ebucore.Resource);
            if (metadata.has("url")) {
                LOG.info("Wikipedia picture : {}", metadata.get("url").asText());
                ebucoreResource.addProperty(Ebucore.locator, model.createTypedLiteral(metadata.get("url").asText(), XSD.anyURI.getURI()));
            }
            if (metadata.has("mime")) {
                Resource mimeType = getMimeTypeIndividual(metadata.get("mime").asText());
                if (mimeType != null) {
                    ebucoreResource.addProperty(Ebucore.hasMimeType, mimeType);
                }
            }
            if (metadata.has("size")) {
                ebucoreResource.addProperty(Ebucore.fileSize, model.createTypedLiteral(metadata.get("size").asInt()));
            }
            if (metadata.has("width")) {
                ebucoreResource.addProperty(Ebucore.width, model.createTypedLiteral(metadata.get("width").asInt()));
            }
            if (metadata.has("height")) {
                ebucoreResource.addProperty(Ebucore.height, model.createTypedLiteral(metadata.get("height").asInt()));
            }
            ebucoreResource = ResourceUtils.renameResource(ebucoreResource, DatatourismeData.resource(JenaUtils.resourceUUID(ebucoreResource)).getURI());

            // ---
            // ebucore:Annotation
            // ---
            Resource ebucoreAnnotation = model.createResource();
            ebucoreAnnotation.addProperty(RDF.type, Ebucore.Annotation);
            // credits
            if (extmetadata.has("Artist")) {
                String artist = fixArtistReference(extmetadata.get("Artist").get("value").asText());
                ebucoreAnnotation.addProperty(Datatourisme.credits, artist);
            } else if (extmetadata.has("Credits")) {
                ebucoreAnnotation.addProperty(Datatourisme.credits, extmetadata.get("Credits").get("value").asText());
            }
            // licence
            if (extmetadata.has("LicenseShortName")) {
                ebucoreAnnotation.addProperty(Ebucore.isCoveredBy, extmetadata.get("LicenseShortName").get("value").asText());
            } else if (extmetadata.has("License")) {
                ebucoreAnnotation.addProperty(Ebucore.isCoveredBy, extmetadata.get("License").get("value").asText());
            }
            // title
            if (extmetadata.has("ObjectName")) {
                ebucoreAnnotation.addProperty(Ebucore.title, extmetadata.get("ObjectName").get("value").asText());
            }
            ebucoreAnnotation = ResourceUtils.renameResource(ebucoreAnnotation, DatatourismeData.resource(JenaUtils.resourceUUID(ebucoreAnnotation)).getURI());

            // final editorial object
            Resource ebucoreEditorialObject = model.createResource();
            ebucoreEditorialObject.addProperty(RDF.type, Ebucore.EditorialObject);
            ebucoreEditorialObject.addProperty(Ebucore.hasRelatedResource, ebucoreResource);
            ebucoreEditorialObject.addProperty(Ebucore.hasAnnotation, ebucoreAnnotation);
            ebucoreEditorialObject = ResourceUtils.renameResource(ebucoreEditorialObject, DatatourismeData.resource(JenaUtils.resourceUUID(ebucoreEditorialObject)).getURI());

            return ebucoreEditorialObject;
        }

        return null;
    }

    /**
     * Transform an artist reference list <a href="href">artist</a> by artist (href)
     *
     * @param artist
     * @return
     */
    private String fixArtistReference(String artist) {
        Pattern p = Pattern.compile("^<a[^>]+href=\\\"([^\\\"]+)\\\"[^>]*>([^<]*)<\\/a>$");
        Matcher m = p.matcher(artist);
        if (m.matches()) {
            return String.format("%s (https:%s)",  m.group(2), m.group(1));
        }
        return artist;
    }

    /**
     * Return corresponding ebucore:MimeType from KB
     * @param mime
     * @return
     */
    private Resource getMimeTypeIndividual(String mime) {
        switch (mime) {
            case "image/jpeg":
                return DatatourismeThesaurus.resource("JPEG");
            case "image/gif":
                return DatatourismeThesaurus.resource("GIF");
            case "image/png":
                return DatatourismeThesaurus.resource("PNG");
            case "image/tiff":
                return DatatourismeThesaurus.resource("TaggedImageFile");
        }
        return null;
    }
}
