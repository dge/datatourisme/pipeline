/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.pipeline.wikidata.utils;

import org.apache.commons.lang3.tuple.Pair;

public class GeoUtils {

    /**
     * Calculate distance between 2 lat/lon points, in meters
     *
     * @param point1
     * @param point2
     * @return
     */
    public static double distance(LatLng point1, LatLng point2) {
        double theta = point1.longitude - point2.longitude;
        double dist = Math.sin(deg2rad(point1.latitude)) * Math.sin(deg2rad(point2.latitude)) + Math.cos(deg2rad(point1.latitude)) * Math.cos(deg2rad(point2.latitude)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515; // miles
        dist = dist * 1.609344 * 1000; // convert to meter
        return (dist);
    }

    /**
     * Parse POINT(1.66 44.5434) to LatLng
     * @param point
     * @return
     */
    public static LatLng parseWKTPoint(String point) {
        double longitude = Double.parseDouble(point.substring(6, point.indexOf(" ")));
        double latitude = Double.parseDouble(point.substring(point.indexOf(" ")+1, point.indexOf(")")));
        return new LatLng(latitude,longitude);
    }

    /* The function to convert decimal into radians */
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /* The function to convert radians into decimal */
    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static LatLng latLng(double latitude, double longitude) {
        return new LatLng(latitude, longitude);
    }

    /**
     * Helper class
     */
    public static class LatLng {
        public final double	latitude;
        public final double	longitude;
        private LatLng(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }
}
